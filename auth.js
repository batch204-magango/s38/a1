const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

//JSON WEB TOKENS

//Token Creation

module.exports.createAccessToken = (user) => {

	console.log(user)
	/*
	{
  _id: new ObjectId("634015b99f0d1fbbf0ecfbdc"),
  firstName: 'Eric',
  lastName: 'Magango',
  email: 'egoy@gmail.com',
  password: '$2b$10$RGAzy0YjT4vcHrdlah21/Ouk5cCagDWnjwMMtlkrRJE3wKJA2Qu2m',
  isAdmin: false,
  mobileNo: '888876',
  enrollments: [],
  __v: 0
}
	*/

	const data = {
		id:user._id,
		email:user.email,
		isAdmin: user.isAdmin
	}

	//Generate a token
		//jwt.sign()
		//{expiresIn: "1m"}
	return jwt.sign(data, secret, {})
}

